package tool;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;

public class DownloadTool {
	
	public static void download(URL url, String destination) throws IOException {
		FileUtils.copyURLToFile(url, new File(destination));
	}
}
