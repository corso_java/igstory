package tool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

public class DateTool {

	public static String DATE_PATTERN = "dd/MM/yyyy";
	public static String DATE_TIME_PATTERN = "dd/MM/yyyy hh:mm:ss";

	public static Date parseIsoDate(String dateTimeText) {
		ZonedDateTime.now().format(DateTimeFormatter.ISO_INSTANT);
		DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
		OffsetDateTime offsetDateTime = OffsetDateTime.parse(dateTimeText, timeFormatter);
		return Date.from(Instant.from(offsetDateTime));
	}

	public static Calendar parseIsoCalendar(String dateTimeText) {
		Date date = parseIsoDate(dateTimeText);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}

	public static String formatDate(Calendar calendar) {
		return format(calendar, DATE_PATTERN);
	}

	public static Calendar parseDate(String string) throws ParseException {
		return parse(string, DATE_PATTERN);
	}

	public static String formatDateTime(Calendar calendar) {
		return format(calendar, DATE_TIME_PATTERN);
	}

	public static Calendar parseDateTime(String string) throws ParseException {
		return parse(string, DATE_TIME_PATTERN);
	}

	private static String format(Calendar calendar, String pattern) {
		return new SimpleDateFormat(pattern).format(calendar.getTime());
	}

	private static Calendar parse(String string, String pattern) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		Calendar calendar = Calendar.getInstance();
		Date date = simpleDateFormat.parse(string);
		calendar.setTime(date);
		return calendar;
	}

}
