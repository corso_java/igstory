package tests;

import java.util.List;

import exceptions.NoInstagramUsernameFound;
import exceptions.PrivateInstagramUsernameException;
import model.InstagramStory;
import model.InstagramUser;
import source.ISource;
import source.StoriesigSource;
import storage.DateTimeFileFormatter;
import storage.IStorage;
import storage.IUsernameSource;
import storage.MyFileStorage;
import storage.UsernameFileSource;

public class MyMain {

	public static void main(String[] args)
			throws NoInstagramUsernameFound, PrivateInstagramUsernameException, Exception {
		IUsernameSource usernameSource = new UsernameFileSource();
		List<InstagramUser> instagramUsers = usernameSource.retrieve();
		ISource source = new StoriesigSource();
		IStorage storage = new MyFileStorage(new DateTimeFileFormatter());
		for (InstagramUser instagramUser : instagramUsers) {
			List<InstagramStory> stories = source.retrieve(instagramUser);
			storage.store(stories);
		}
	}
}
