package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.util.List;

import org.junit.jupiter.api.Test;

import exceptions.NoInstagramUsernameFound;
import exceptions.PrivateInstagramUsernameException;
import model.InstagramStory;
import model.InstagramUser;
import model.MediaStory.MediaType;
import source.ISource;
import source.StoriesigSource;
import storage.IUsernameSource;
import storage.UsernameFileSource;

class Test01 {

	@Test
	void test() throws Exception {
		File file = new File("./chiaraferragni.html");
		ISource source = new StoriesigSource(file);
		List<InstagramStory> result = source.retrieve(new InstagramUser("chiaraferragni"));
		int expected = 24;
		assertTrue("Il metodo non ritorna il numero corretto", expected == result.size());
	}

	/**
	 * Questo test serve per verificare che l'oggetto ISource recuperi correttamente
	 * sia immagini che video
	 * 
	 * @throws Exception
	 */
	@Test
	void testCountTypeOfMedia() throws Exception {
		File file = new File("./chiaraferragni.html");
		ISource source = new StoriesigSource(file);
		List<InstagramStory> result = source.retrieve(new InstagramUser("chiaraferragni"));
		int imageExpceted = 19;
		int videoExpceted = 5;
		int imageDetected = 0;
		for (InstagramStory instagramStory : result) {
			if (instagramStory.getMediaStory().getMediaType() != null
					&& instagramStory.getMediaStory().getMediaType() == MediaType.IMAGE) {
				imageDetected++;
			}
		}
		assertTrue("Il metodo non ritorna il numero di immagini correttamente", imageExpceted == imageDetected);
		int videoDetected = 0;
		for (InstagramStory instagramStory : result) {
			if (instagramStory.getMediaStory().getMediaType() != null
					&& instagramStory.getMediaStory().getMediaType() == MediaType.VIDEO) {
				videoDetected++;
			}
		}
		assertTrue("Il metodo non ritorna il numero di video correttamente", videoExpceted == videoDetected);
	}

	@Test
	void testRetrieveFromUrl() throws Exception {
		ISource source = new StoriesigSource();
		List<InstagramStory> instagramStories = source.retrieve(new InstagramUser("chiaraferragni"));
		int imageDetected = 0;
		for (InstagramStory instagramStory : instagramStories) {
			if (instagramStory.getMediaStory().getMediaType() != null
					&& instagramStory.getMediaStory().getMediaType() == MediaType.IMAGE) {
				imageDetected++;
			}
		}
		int videoDetected = 0;
		for (InstagramStory instagramStory : instagramStories) {
			if (instagramStory.getMediaStory().getMediaType() != null
					&& instagramStory.getMediaStory().getMediaType() == MediaType.VIDEO) {
				videoDetected++;
			}
		}
		int expected = videoDetected + imageDetected;
		assertTrue("Il numero delle storie non è corretto", expected == instagramStories.size());
	}

	@Test
	public void testNoUserFound() throws Exception {
		try {
			ISource source = new StoriesigSource();
			List<InstagramStory> instagramStories = source.retrieve(new InstagramUser("wiuehf8hfhwef8hwe89fy"));
			fail("Il metodo non funziona correttamente");
		} catch (NoInstagramUsernameFound e) {

		} catch (PrivateInstagramUsernameException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testPrivateInstagramUsername() throws Exception {
		ISource source = new StoriesigSource();
		try {
			source.retrieve(new InstagramUser("couplesnote"));
			fail("Il metodo non funziona correttamente");
		} catch (NoInstagramUsernameFound e) {
			fail("Il profilo esiste ma è privato!");
			e.printStackTrace();
		} catch (PrivateInstagramUsernameException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testUsernameSource() {
		IUsernameSource usernameSource = new UsernameFileSource();
		int expected = 2;
		assertTrue("Il metodo non funziona correttamente", expected == usernameSource.retrieve().size());
	}
}
