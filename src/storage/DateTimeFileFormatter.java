package storage;

import model.InstagramStory;
import model.MediaStory.MediaType;
import tool.DateTool;

public class DateTimeFileFormatter implements IMediaFileFormatter {

	@Override
	public String createMediaFileName(InstagramStory instagramStory) {
		String name = DateTool.formatDateTime(instagramStory.getPublishedAt());
		String extension = null;
		MediaType mediaType = instagramStory.getMediaStory().getMediaType();
		if (mediaType.equals(MediaType.IMAGE)) {
			extension = ".jpg";
		}
		if (mediaType.equals(MediaType.VIDEO)) {
			extension = ".mp4";
		}
		return name + extension;
	}
}
