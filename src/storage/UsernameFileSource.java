package storage;

import java.util.ArrayList;
import java.util.List;

import model.InstagramUser;
import tool.ReadFile;

public class UsernameFileSource implements IUsernameSource {

	@Override
	public List<InstagramUser> retrieve() {
		List<InstagramUser> result = new ArrayList<>();
		ReadFile file = new ReadFile();
		List<String> list = file.readFile("./usernames.txt");
		for (String string : list) {
			result.add(new InstagramUser(string));
		}
		return result;
	}

}
