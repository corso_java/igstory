package storage;

import java.util.List;

import model.InstagramStory;

public interface IStorage {
	
	public void store(List<InstagramStory> stories) throws Exception;

}
