package storage;

import java.util.List;

import model.InstagramUser;

public interface IUsernameSource {
	/**
	 * Questo metodo mi permette di recuperare la lista degli utenti instagram,
	 * da cui voglio recuperare/scaricare le mie storie
	 * @return
	 */
	public List<InstagramUser> retrieve();

}
