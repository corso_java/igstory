package storage;

import model.InstagramStory;

public interface IMediaFileFormatter {
	
	public String createMediaFileName(InstagramStory instagramStory);

}
