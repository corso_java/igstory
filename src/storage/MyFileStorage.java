package storage;

import java.io.File;
import java.util.List;

import model.InstagramStory;
import tool.DownloadTool;

public class MyFileStorage implements IStorage {

	private static String FOLDER_PATH = "./";
	private IMediaFileFormatter mediaFileFormatter;

	public MyFileStorage(IMediaFileFormatter mediaFileFormatter) {
		super();
		this.mediaFileFormatter = mediaFileFormatter;
	}

	@Override
	public void store(List<InstagramStory> stories) throws Exception {
		for (InstagramStory instagramStory : stories) {
			String userFolderPath = FOLDER_PATH + instagramStory.getUsername() + "/";
			checkFolder(userFolderPath);
			String destionation = userFolderPath + mediaFileFormatter.createMediaFileName(instagramStory);
			DownloadTool.download(instagramStory.getMediaStory().getUrl(), destionation);
		}
	}

	private void checkFolder(String userFolderPath) {
		File file = new File(userFolderPath);
		if (!file.exists()) {
			file.mkdirs();
		}
	}
}
