package exceptions;

public class NoInstagramUsernameFound extends Exception {

	private String username;

	public NoInstagramUsernameFound(String username) {
		super();
		this.username = username;
	}

}
