package source;

import java.util.Calendar;

import org.jsoup.nodes.Element;

public interface ITimeParser {
	
	public Calendar parse(Element element);

}
