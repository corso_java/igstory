package source;

import java.util.Calendar;

import org.jsoup.nodes.Element;

import tool.DateTool;

public class TimeMediaParser implements ITimeParser {

	@Override
	public Calendar parse(Element element) {
		String dateTimeText = element.select("time").first().attr("dateTime");
		Calendar calendar = DateTool.parseIsoCalendar(dateTimeText);
		return calendar;
	}

}
