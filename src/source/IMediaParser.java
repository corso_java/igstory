package source;

import java.net.MalformedURLException;

import org.jsoup.nodes.Element;

import model.MediaStory;

public interface IMediaParser {
	
	public MediaStory parse(Element element) throws MalformedURLException;

}
