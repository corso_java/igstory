package source;

import java.net.MalformedURLException;
import java.net.URL;

import org.jsoup.nodes.Element;

import model.MediaStory;
import model.MediaStory.MediaType;

public class ImageMediaParser implements IMediaParser {

	@Override
	public MediaStory parse(Element element) throws MalformedURLException {
		URL url = new URL(element.select("div.download > a").first().attr("href"));
		return new MediaStory(url, MediaType.IMAGE);
	}

}
