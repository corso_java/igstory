package source;

import java.util.List;

import exceptions.NoInstagramUsernameFound;
import exceptions.PrivateInstagramUsernameException;
import model.InstagramStory;
import model.InstagramUser;

public interface ISource {
	
	public List<InstagramStory> retrieve(InstagramUser instagramUser) throws NoInstagramUsernameFound, PrivateInstagramUsernameException, Exception;

}
