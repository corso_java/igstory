package source;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import exceptions.NoInstagramUsernameFound;
import exceptions.PrivateInstagramUsernameException;
import model.InstagramStory;
import model.InstagramUser;
import model.MediaStory;
import tool.SSLFixer;

public class StoriesigSource implements ISource {

	private File input;
	public static String URL_STORIESIG = "https://storiesig.com/stories/";
	private HashMap<String, IMediaParser> mediaParsers;
	private ITimeParser timeParser;
	private static int CONN_TIMEOUT = 10000;

	public StoriesigSource(File input) {
		super();
		this.input = input;
		setup();
	}

	public StoriesigSource() {
		super();
		setup();
	}

	private void setup() {
		mediaParsers = new HashMap<>();
		mediaParsers.put("img", new ImageMediaParser());
		mediaParsers.put("video", new VideoMediaParser());
		this.timeParser = new TimeMediaParser();
	}

	@Override
	public List<InstagramStory> retrieve(InstagramUser instagramUser)
			throws NoInstagramUsernameFound, PrivateInstagramUsernameException, Exception {
		List<InstagramStory> result = new ArrayList<>();
		Document doc = null;
		if (input != null) {
			try {
				doc = Jsoup.parse(input, "UTF-8");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			doc = onlineSource(instagramUser);
		}
		Elements articles = doc.select("article");
		for (Element element : articles) {
			String mediaTypeTag = element.getAllElements().get(1).tagName();
			MediaStory mediaStory = null;
			try {
				mediaStory = mediaParsers.get(mediaTypeTag).parse(element);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			Calendar publishedAt = timeParser.parse(element);
			result.add(new InstagramStory(mediaStory, publishedAt, instagramUser.getUsername()));
		}
		return result;
	}

	private Document onlineSource(InstagramUser instagramUser)
			throws NoInstagramUsernameFound, PrivateInstagramUsernameException, MalformedURLException, IOException {
		new SSLFixer().fix();
		Document profileDocument = Jsoup
				.parse(new URL("https://storiesig.com/?username=" + instagramUser.getUsername()), CONN_TIMEOUT);
		Elements elements = profileDocument.select("a.user").select("p");
		if (elements.size() != 0) {
			String p = elements.get(0).text();
			if (p.equalsIgnoreCase("This Account is Private")) {
				throw new PrivateInstagramUsernameException(instagramUser.getUsername());
			} else {
				return retrieveFromUrl(instagramUser.getUsername());
			}
		} else {
			throw new NoInstagramUsernameFound(instagramUser.getUsername());
		}
	}

	private Document retrieveFromUrl(String username) throws MalformedURLException, IOException {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(URL_STORIESIG);
		stringBuilder.append(username);
		return Jsoup.parse(new URL(stringBuilder.toString()), CONN_TIMEOUT);
	}
}
