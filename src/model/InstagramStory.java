package model;

import java.util.Calendar;

public class InstagramStory {

	private MediaStory mediaStory;
	private Calendar publishedAt;
	private String username;

	public InstagramStory(MediaStory mediaStory, Calendar publishedAt, String username) {
		super();
		this.mediaStory = mediaStory;
		this.publishedAt = publishedAt;
		this.username = username;
	}

	public InstagramStory() {
		super();
	}

	public MediaStory getMediaStory() {
		return mediaStory;
	}

	public void setMediaStory(MediaStory mediaStory) {
		this.mediaStory = mediaStory;
	}

	public Calendar getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(Calendar publishedAt) {
		this.publishedAt = publishedAt;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return String.format("InstagramStory [mediaStory=%s, publishedAt=%s, username=%s]", mediaStory, publishedAt,
				username);
	}

}
