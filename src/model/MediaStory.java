package model;

import java.net.URL;

public class MediaStory {

	private URL url;
	private MediaType mediaType;

	public MediaStory(URL url, MediaType mediaType) {
		super();
		this.url = url;
		this.mediaType = mediaType;
	}

	public MediaStory() {
		super();
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

	public enum MediaType {
		IMAGE, VIDEO;
	}

}
