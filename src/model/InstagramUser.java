package model;

public class InstagramUser {

	private String username;
	private InstagramVisibility visibility;

	public InstagramUser(String username, InstagramVisibility visibility) {
		super();
		this.username = username;
		this.visibility = visibility;
	}

	public InstagramUser(String username) {
		super();
		this.username = username;
	}

	public InstagramUser() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public InstagramVisibility getVisibility() {
		return visibility;
	}

	public void setVisibility(InstagramVisibility visibility) {
		this.visibility = visibility;
	}

	public enum InstagramVisibility {

		PRIVATE, PUBLIC;
	}

	@Override
	public String toString() {
		return String.format("InstagramUser [username=%s, visibility=%s]", username, visibility);
	}

}
